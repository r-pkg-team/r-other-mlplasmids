#' @title Prediction of plasmid-derived contigs assembled by short-read data for a selection of single-species
#' @description Machine-learning classifiers to predict plasmid- or chromosome- derived short-read contigs for a selection single of species
#' @param path_input_file Name of the fasta file with the contig sequences.
#' If the name does not contain relative or full path then the name of the file will be relative to current working directory.
#' @param species Character value. Species with plasmid classifiers available (Enterococcus faecium).
#' @param prob_threshold  Integer value ranging from 0 to 1. Only predicted plasmid contigs with a probability higher than given value are reported. Default value = 0.5
#' @param full_output Boolean value. If TRUE also predicted chromosome-derived contigs are reported. Default value = FALSE
#' @param min_length Integer value. Minimum contig length (bp) considered to report the prediction. Default value = 1000
#' @return Dataframe reporting probabilities and class prediction for each contig
#' @author Sergio Arredondo-Alonso ; Dr. Anita C. Schürch
#' @examples plasmid_classification(path_input_file = 'spades_contigs.fasta')
#' @examples plasmid_classification(path_input_file = 'spades_contigs.fasta', prob_threshold = 0.7)
#' @examples plasmid_classification(path_input_file = 'spades_contigs.fasta', full_output = TRUE)
#' @export
#'


plasmid_classification <- function(path_input_file,  species, prob_threshold = 0.5, full_output = FALSE, min_length = 1000)
  {
    # Check that user specifies two mandatory arguments

    ## Checking that user specifies the path to the contigs file
    if(missing(path_input_file))
    {
      stop("Please specify a path containning the input file")
    }
    ## Checking that user specifies the bacterial species
    if(missing(species) | ! species %in% c('Enterococcus faecium','Klebsiella pneumoniae', 'Escherichia coli'))
    {
      stop("Please specify one of the following species: 'Enterococcus faecium' 'Klebsiella pneumoniae' 'Escherichia coli' ")
    }

    if(min_length < 1000)
    {
      warning('!!! Be aware that predictions for sequences < 1000 bp can be misleading !!!')
    }

    if(prob_threshold > 0 & prob_threshold < 1)
    {
      if(species == 'Enterococcus faecium')
      {
        user_file <- readDNAStringSet(filepath = path_input_file, format="fasta")
        pentamer_freq <- as.data.frame(oligonucleotideFrequency(user_file, width=5,as.prob = TRUE, with.labels = T))
        prediction_svm <- predict(faecium_svm_model,newdata = pentamer_freq)
        prediction_data <- prediction_svm$data
        prediction_data$Contig_Name <- names(user_file)
        prediction_data$Contig_Length <- nchar(as.character(paste(user_file)))
        colnames(prediction_data) <- c("Prob_Chromosome","Prob_Plasmid","Prediction","Contig_name","Contig_length")

        if(full_output != TRUE)
        {
          prediction_data <- subset(prediction_data,prediction_data$Prob_Plasmid > prob_threshold)
          prob_chr_threshold <-
          prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
          return(prediction_data)
        }
        else
        {
          prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
          return(prediction_data)
        }
       }

        if(species == 'Klebsiella pneumoniae')
        {
          if(prob_threshold > 0 & prob_threshold < 1)
          {
            user_file <- readDNAStringSet(filepath = path_input_file, format="fasta")
            pentamer_freq <- as.data.frame(oligonucleotideFrequency(user_file, width=5,as.prob = TRUE, with.labels = T))
            prediction_svm <- predict(pneumoniae_svm_model,newdata = pentamer_freq)
            prediction_data <- prediction_svm$data
            prediction_data$Contig_Name <- names(user_file)
            prediction_data$Contig_Length <- nchar(as.character(paste(user_file)))
            colnames(prediction_data) <- c("Prob_Chromosome","Prob_Plasmid","Prediction","Contig_name","Contig_length")

            if(full_output != TRUE)
            {
              prediction_data <- subset(prediction_data,prediction_data$Prob_Plasmid > prob_threshold)
              prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
              return(prediction_data)
            }
            else
            {
              prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
              return(prediction_data)
            }

          }
        }
      if(species == 'Escherichia coli')
      {
        if(prob_threshold > 0 & prob_threshold < 1)
        {
          user_file <- readDNAStringSet(filepath = path_input_file, format="fasta")
          pentamer_freq <- as.data.frame(oligonucleotideFrequency(user_file, width=5,as.prob = TRUE, with.labels = T))
          prediction_svm <- predict(ecoli_svm_model,newdata = pentamer_freq)
          prediction_data <- prediction_svm$data
          prediction_data$Contig_Name <- names(user_file)
          prediction_data$Contig_Length <- nchar(as.character(paste(user_file)))
          colnames(prediction_data) <- c("Prob_Chromosome","Prob_Plasmid","Prediction","Contig_name","Contig_length")

          if(full_output != TRUE)
          {
            prediction_data <- subset(prediction_data,prediction_data$Prob_Plasmid > prob_threshold)
            prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
            warning('This Escherichia coli classifier is still being tested')
            return(prediction_data)
          }
          else
          {
            prediction_data <- subset(prediction_data, as.numeric(as.character(prediction_data$Contig_length)) > min_length)
            warning('This Escherichia coli classifier is still being tested')
            return(prediction_data)

          }

        }
      }

      else
      {
        stop('Please select a posterior probability threshold ranging from 0 to 1')
      }

      }

}
